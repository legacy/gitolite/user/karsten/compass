var compassModule = angular.module("Compass", ['ui','oblique.directives','oblique.filters'])

compassModule.value('ui.config', {
   select2: {
      allowClear: true,
      width: "element",
   }
});

compassModule.controller('CompassCtrl',function CompassCtrl($scope,$http,$location) {

  $scope.state = "hidden"
  $scope.query = {
  }

  /** Watch the location bar to allow us to load saved searches **/
  $scope.$watch($location.search(), function(newv, oldv, scope) {
    if ($location.search().top) {
      $scope.query=$location.search()
      $scope.request()
    }
  })

  /**  Make a data request from the form
   *
   * Call 'success_cb' if the request is successful
   */
  $scope.request = function(success_cb) {
    var rs_query = "";

    if ((typeof $scope.query["country"]) !== "undefined" && $scope.query["country"] !== "") {
      rs_query += "country:" + $scope.query["country"] + " ";
    }
    if ((typeof $scope.query["ases"]) !== "undefined" && $scope.query["ases"] !== "") {
      rs_query += "as:" + $scope.query["ases"] + " ";
    }
    if ((typeof $scope.query["family"]) !== "undefined" && $scope.query["family"] !== "") {
      rs_query += "family:" + $scope.query["family"] + " ";
    }
    if ((typeof $scope.query["exits_only"]) !== "undefined" && $scope.query["exits_only"] == true) {
      rs_query += "flag:exit" + " ";
    } else if ((typeof $scope.query["guards_only"]) !== "undefined" && $scope.query["guards_only"] == true) {
      rs_query += "flag:guard" + " ";
    }

    var by_country = ((typeof $scope.query["by_country"]) !== "undefined" && $scope.query["by_country"] == true);
    var by_as = ((typeof $scope.query["by_as"]) !== "undefined" && $scope.query["by_as"] == true);

    var rs_url = "https://atlas.torproject.org/";

    if (!by_country) {
      if (!by_as) {
        rs_url += "#search/running:true " + rs_query.trim();
      } else {
        rs_url += "#aggregate/as/" + rs_query.trim();
      }
    } else {
      if (!by_as) {
        rs_url += "#aggregate/cc/" + rs_query.trim();
      } else {
        rs_url += "#aggregate/ascc/" + rs_query.trim();
      }
    }

    document.location = rs_url;
  };

  $scope.reset = function() {
    $scope.state="hidden"
  }

  $http.get("static/data/cc.json").success(function(data) {
    $scope.cc_data = data
  })

  $scope.country_select = {
    allowClear: true,
    width: "element",
  }

})
